
public class Cat {

    private Position position;
    private boolean escaped;
    
    
    public Cat(Position pos)
    {
        this.position = pos;
        this.escaped = false;        
    }

    public Position getPosition() {
        return position;
    }


    public void setPosition(Position position) {
        this.position = position;
    }


    public boolean isEscaped() {
        return escaped;
    }


    public void setEscaped(boolean escaped) {
        this.escaped = escaped;
    }
    
    
}
