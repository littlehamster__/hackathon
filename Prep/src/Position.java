
public class Position {

    //from 0 to (width or height) - 1.
    private int column;
    private int row;
    

    
    public Position (int x, int y)
    {
        this.column = x;
        this.row = y;
    }
    public int getColumn() {
        return column;
    }

    public void setColumn(int x) {
        this.column = x;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int y) {
        this.row = y;
    }

}
