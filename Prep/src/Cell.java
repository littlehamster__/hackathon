
public class Cell {

    private boolean isFree;
    private boolean isCat;
    private Position position;

    public Cell(Position position)
    {
        this.position = position;
        this.isFree = true;
        this.isCat = false;
        
    }
    
    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean isFree) {
        this.isFree = isFree;
    }

    public boolean isCat() {
        return isCat;
    }

    public void setCat(boolean isCat) {
        this.isCat = isCat;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
   
}
