import java.util.LinkedList;

public class Field {

	private int width;
	private int height;
	private LinkedList<Direction> l;
	private LinkedList<Cell> cells;
	private int help;
	private Cat cat;

	public Field(int width, int height) {
		this.width = width;
		this.height = height;
		this.initCells();
		this.randomFill();

		this.cat = new Cat(this.getCenter());
		this.locateCat(this.getCenter());

	}

	public void initCells() {
		this.cells = new LinkedList<Cell>();
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Cell cell = new Cell(new Position(i, j));
				this.cells.add(cell);
			}
		}

	}

	/**
	 * returns the cell according to the position, which range from (0,0) to
	 * (width - 1, height - 1).
	 * 
	 * @param position
	 * @return
	 */
	public Cell getCell(Position position) {
		return cells.get(position.getRow() * this.width + position.getColumn());
	}

	public void locateCat(Position pos) {
		this.getCell(pos).setCat(true);
		this.getCell(pos).setFree(false);
	}

	public void moveCat(Position oldPos, Position newPos) {
		this.getCell(oldPos).setCat(false);
		this.getCell(oldPos).setFree(true);
		this.locateCat(newPos);
	}

	public void randomFill() {
		for (int i = 0; i < (this.height * this.width) / 10; i++) {
			int num = (int) (Math.random() * cells.size());
			cells.get(num).setFree(false);
		}
	}
	
	public void escape() {
		int northDist = this.cat.getPosition().getRow();
		int westDist = this.cat.getPosition().getColumn();
		int southDist = this.height - this.cat.getPosition().getRow() - 1;
		int eastDist = width - this.cat.getPosition().getColumn() - 1;
		Direction dir1 = null;
		int min = 2000;
		Direction dir = null;
		switch ((int) (Math.random() * 4 + 1)) {
		case 1:
			if (northDist < min) {
				min = northDist;
				dir = Direction.UP;
			}
			if (westDist < min) {
				min = westDist;
				dir = Direction.LEFT;
			}
			if (southDist < min) {
				min = southDist;
				dir = Direction.DOWN;
			}
			if (eastDist < min) {
				min = eastDist;
				dir = Direction.RIGHT;
			}
			break;
		case 2:
			if (northDist < min) {
				min = northDist;
				dir = Direction.UP;
			}
			if (westDist < min) {
				min = westDist;
				dir = Direction.LEFT;
			}
			if (eastDist < min) {
				min = eastDist;
				dir = Direction.RIGHT;
			}
			if (southDist < min) {
				min = southDist;
				dir = Direction.DOWN;
			}
			break;
		case 3:
			if (northDist < min) {
				min = northDist;
				dir = Direction.UP;
			}
			if (southDist < min) {
				min = southDist;
				dir = Direction.DOWN;
			}
			if (eastDist < min) {
				min = eastDist;
				dir = Direction.RIGHT;
			}
			if (westDist < min) {
				min = westDist;
				dir = Direction.LEFT;
			}
			break;
		case 4:
			if (westDist < min) {
				min = westDist;
				dir = Direction.LEFT;
			}
			if (southDist < min) {
				min = southDist;
				dir = Direction.DOWN;
			}
			if (eastDist < min) {
				min = eastDist;
				dir = Direction.RIGHT;
			}
			if (northDist < min) {
				min = northDist;
				dir = Direction.UP;
			}
			break;
		}
		l = new LinkedList<Direction>();
		help = 0;
		this.shortestWay(cat.getPosition(), dir);
		Position moveTo = cat.getPosition();
		if (l.size() != 0)
		dir1 = (Direction) l.getFirst();
		if (dir1 == Direction.DOWN) {
			moveTo.setRow(moveTo.getRow() + 1);
		} else if (dir1 == Direction.DOWNLEFT) {
			moveTo.setColumn(moveTo.getColumn() - 1);
			moveTo.setRow(moveTo.getRow() + 1);
		} else if (dir1 == Direction.DOWNRIGHT) {
			moveTo.setColumn(moveTo.getColumn() + 1);
			moveTo.setRow(moveTo.getRow() + 1);
		} else if (dir1 == Direction.LEFT) {
			moveTo.setColumn(moveTo.getColumn() - 1);
		} else if (dir1 == Direction.RIGHT) {
			moveTo.setColumn(moveTo.getColumn() + 1);
		} else if (dir1 == Direction.UP) {
			moveTo.setRow(moveTo.getRow() - 1);
		} else if (dir1 == Direction.UPLEFT) {
			moveTo.setColumn(moveTo.getColumn() - 1);
			moveTo.setRow(moveTo.getRow() - 1);
		} else if (dir1 == Direction.UPRIGHT) {
			moveTo.setRow(moveTo.getRow() - 1);
			moveTo.setColumn(moveTo.getColumn() + 1);
		} else {
			switch ((int) (Math.random() * 8 + 1)) {
			case 1:
				moveTo.setRow(moveTo.getRow() + 1);
				break;
			case 2:
				moveTo.setColumn(moveTo.getColumn() - 1);
				moveTo.setRow(moveTo.getRow() + 1);
				break;
			case 3:
				moveTo.setColumn(moveTo.getColumn() + 1);
				moveTo.setRow(moveTo.getRow() + 1);
				break;
			case 4:
				moveTo.setColumn(moveTo.getColumn() - 1);
				break;
			case 5: 
				moveTo.setColumn(moveTo.getColumn() + 1);
				break;
			case 6:
				moveTo.setRow(moveTo.getRow() - 1);
				break;
			case 7:
				moveTo.setColumn(moveTo.getColumn() - 1);
				moveTo.setRow(moveTo.getRow() - 1);
				break;
			case 8:
				moveTo.setRow(moveTo.getRow() - 1);
				moveTo.setColumn(moveTo.getColumn() + 1);
				break;
			}
		}
		this.moveCat(this.cat.getPosition() , moveTo);
	}

	public void shortestWay(Position p, Direction dir) {
		boolean left = true, right = true, front = true, done = false;
		help++;
		Direction o = dir;
		Position originP = p;
		while (l.size() == 0) {
			if (dir == Direction.LEFT) {
				if (front) {
					p.setColumn(p.getColumn() - 1);
					if (p.getColumn() >= 0) {
						if (getCell(p).isFree() == true) {
							shortestWay(p, o);
						}
					} else {
						done = true;
					}
					if (done == true) {
						l.add(dir);
						break;
					}
					front = false;
					break;
				} else if (left) {
					if (p.getRow() % 2 != 0) {
						p.setRow(p.getRow() - 1);
						p.setColumn(p.getColumn() - 1);
					} else {
						p.setRow(p.getRow() - 1);
					}
					dir = Direction.DOWN;
					if (p.getColumn() >= 0) {
						if (p.getRow() < this.height && getCell(p).isFree() == true) {
							shortestWay(p, o);
						}
					} else {
						done = true;
					}
					if (done == true) {
						l.add(dir);
						break;
					}
					left = false;
					break;
				} else if (right) {
					if (p.getRow() % 2 != 0) {
						p.setRow(p.getRow() + 1);
						p.setColumn(p.getColumn() - 1);
					} else {
						p.setRow(p.getRow() + 1);
					}
					dir = Direction.UP;
					if (p.getColumn() >= 0) {
						if (p.getRow() >= 0) {
							shortestWay(p, o);
						}
					} else {
						done = true;
					}
					if (done == true) {
						l.add(dir);
						break;
					}
					right = false;
					break;
				}
			} else if (dir == Direction.RIGHT) {
				if (front) {
					p.setColumn(p.getColumn() + 1);
					if (p.getColumn() <= this.width) {
						if (getCell(p).isFree() == true) {
							shortestWay(p, o);
						}
					} else {
						done = true;
					}
					if (done == true) {
						l.add(dir);
						break;
					}
					front = false;
					break;
				} else if (left) {
					if (p.getRow() % 2 != 0) {
						p.setRow(p.getRow() + 1);
						p.setColumn(p.getColumn() + 1);
					} else {
						p.setRow(p.getRow() + 1);
					}
					dir = Direction.UP;
					if (p.getColumn() < this.width && p.getRow() >= 0) {
						if (getCell(p).isFree() == true) {
							shortestWay(p, o);
						}
					} else {
						done = true;
					}
					if (done == true) {
						l.add(dir);
						break;
					}
					left = false;
					break;
				} else if (right) {
					if (p.getRow() % 2 != 0) {
						p.setRow(p.getRow() + 1);
						p.setColumn(p.getColumn() - 1);
					} else {
						p.setRow(p.getRow() + 1);
					}
					dir = Direction.DOWN;
					if (p.getColumn() < this.width && p.getRow() < this.height) {
						if (getCell(p).isFree() == true) {
							shortestWay(p, o);
						}
					} else {
						done = true;
					}
					if (done == true) {
						l.add(dir);
						break;
					}
					right = false;
					break;
				}
			} else if (dir == Direction.UP) {
				if (p.getColumn() - 1 > this.width - p.getColumn() - 1) {
					if (p.getRow() % 2 != 0) {
						dir = Direction.UPRIGHT;
					} else {
						dir = Direction.UP;
					}
				} else if (p.getColumn() - 1 < this.width - p.getColumn() - 1) {
					if (p.getRow() % 2 != 0) {
						dir = Direction.UP;
					} else {
						dir = Direction.UPLEFT;
					}
				} else {
					if (p.getRow() % 2 != 0) {
						switch ((int) (Math.random() * 2 + 1)) {
						case 1:
							dir = Direction.UP;
							break;
						case 2:
							dir = Direction.UPRIGHT;
							break;
						}
					} else {
						switch ((int) (Math.random() * 2 + 1)) {
						case 1:
							dir = Direction.UP;
							break;
						case 2:
							dir = Direction.UPLEFT;
							break;
						}
					}
				}
				if (dir == Direction.UP) {
					if (front) {
						if (p.getRow() % 2 != 0) {
							p.setRow(p.getRow() - 1);
							dir = Direction.UPLEFT;
						} else {
							p.setRow(p.getRow() - 1);
							dir = Direction.UPRIGHT;
						}
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() >= 0) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						front = false;
						break;
					} else if (left) {
						if (p.getRow() % 2 != 0) {
							p.setColumn(p.getColumn() - 1);
							dir = Direction.LEFT;
						} else {
							p.setRow(p.getRow() - 1);
							p.setColumn(p.getColumn() - 1);
							dir = Direction.UPLEFT;
						}
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() >= 0) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						left = false;
						break;
					} else if (right) {
						if (p.getRow() % 2 != 0) {
							p.setRow(p.getRow() - 1);
							p.setColumn(p.getColumn() + 1);
							dir = Direction.UPRIGHT;
						} else {
							p.setColumn(p.getColumn() + 1);
							dir = Direction.RIGHT;
						}
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() >= 0) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						right = false;
						break;
					}
				} else if (dir == Direction.UPRIGHT) {
					if (front) {
						p.setRow(p.getRow() - 1);
						p.setColumn(p.getColumn() + 1);
						dir = Direction.UP;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() >= 0) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						front = false;
						break;
					} else if (left) {
						p.setRow(p.getRow() - 1);
						dir = Direction.UP;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() >= 0) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						left = false;
						break;
					} else if (right) {
						p.setColumn(p.getColumn() + 1);
						dir = Direction.RIGHT;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() >= 0) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						right = false;
						break;
					}
				} else {
					if (front) {
						p.setColumn(p.getColumn() - 1);
						p.setRow(p.getRow() - 1);
						dir = Direction.UP;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() >= 0) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						front = false;
						break;
					} else if (left) {
						p.setColumn(p.getColumn() - 1);
						dir = Direction.LEFT;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() >= 0) {
							if (getCell(p).isFree() == true) {
								if (getCell(p).isFree() == true) {
									shortestWay(p, o);
								}
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						left = false;
						break;
					} else if (right) {
						p.setRow(p.getRow() - 1);
						dir = Direction.UP;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() >= 0) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						right = false;
						break;
					}
				}
			} else if (dir == Direction.DOWN) {
				if (p.getColumn() - 1 > this.width - p.getColumn() - 1) {
					if (p.getRow() % 2 != 0) {
						dir = Direction.DOWNRIGHT;
					} else {
						dir = Direction.DOWN;
					}
				} else if (p.getColumn() - 1 < this.width - p.getColumn() - 1) {
					if (p.getRow() % 2 != 0) {
						dir = Direction.DOWN;
					} else {
						dir = Direction.DOWNLEFT;
					}
				} else {
					if (p.getRow() % 2 != 0) {
						switch ((int) (Math.random() * 2 + 1)) {
						case 1:
							dir = Direction.DOWN;
							break;
						case 2:
							dir = Direction.DOWNRIGHT;
							break;
						}
					} else {
						switch ((int) (Math.random() * 2 + 1)) {
						case 1:
							dir = Direction.DOWN;
							break;
						case 2:
							dir = Direction.DOWNLEFT;
							break;
						}
					}
				}
				if (dir == Direction.DOWN) {
					if (front) {
						if (p.getRow() % 2 != 0) {
							p.setRow(p.getRow() + 1);
							dir = Direction.DOWNLEFT;
						} else {
							p.setRow(p.getRow() + 1);
							dir = Direction.DOWNRIGHT;
						}
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() < this.height) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						front = false;
						break;
					} else if (left) {
						if (p.getRow() % 2 != 0) {
							p.setColumn(p.getColumn() - 1);
							dir = Direction.DOWN;
						} else {
							p.setRow(p.getRow() + 1);
							p.setColumn(p.getColumn() - 1);
							dir = Direction.DOWNLEFT;
						}
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() < this.height) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						left = false;
						break;
					} else if (right) {
						if (p.getRow() % 2 != 0) {
							p.setColumn(p.getColumn() - 1);
							dir = Direction.DOWNRIGHT;
						} else {
							p.setRow(p.getRow() + 1);
							p.setColumn(p.getColumn() - 1);
							dir = Direction.RIGHT;
						}
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() < this.height) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						right = false;
						break;
					}
				} else if (dir == Direction.DOWNRIGHT) {
					if (front) {
						p.setRow(p.getRow() + 1);
						dir = Direction.DOWN;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() < this.height) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						front = false;
						break;
					} else if (left) {
						p.setColumn(p.getColumn() + 1);
						dir = Direction.DOWN;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() < this.height) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						left = false;
						break;
					} else if (right) {
						p.setRow(p.getRow() + 1);
						dir = Direction.DOWN;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() < this.height) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						right = false;
						break;
					}
				} else {
					if (front) {
						p.setColumn(p.getColumn() - 1);
						p.setRow(p.getRow() + 1);
						dir = Direction.DOWN;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() < this.height) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						front = false;
						break;
					} else if (left) {
						p.setColumn(p.getColumn() - 1);
						dir = Direction.LEFT;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() < this.height) {
							if (getCell(p).isFree() == true) {
								if (getCell(p).isFree() == true) {
									shortestWay(p, o);
								}
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						left = false;
						break;
					} else if (right) {
						p.setColumn(p.getColumn() - 1);
						dir = Direction.DOWN;
						if (p.getColumn() < this.width && p.getColumn() >= 0 && p.getRow() < this.height) {
							if (getCell(p).isFree() == true) {
								shortestWay(p, o);
							}
						} else {
							done = true;
						}
						if (done == true) {
							l.add(dir);
							break;
						}
						right = false;
						break;
					}
				}
			}
			if (help >= 4) {
				break;
			}
			if (front == false && left == false && right == false) {
				if (o == Direction.LEFT) {
					switch ((int) (Math.random() * 2 + 1)) {
					case 1:
						o = Direction.UP;
						break;
					case 2:
						o = Direction.DOWN;
						break;
					}
				} else if (o == Direction.RIGHT) {
					switch ((int) (Math.random() * 2 + 1)) {
					case 1:
						o = Direction.UP;
						break;
					case 2:
						o = Direction.DOWN;
						break;
					}
				} else if (o == Direction.UP) {
					switch ((int) (Math.random() * 2 + 1)) {
					case 1:
						o = Direction.LEFT;
						break;
					case 2:
						o = Direction.RIGHT;
						break;
					}
				} else {
					switch ((int) (Math.random() * 2 + 1)) {
					case 1:
						o = Direction.LEFT;
						break;
					case 2:
						o = Direction.RIGHT;
						break;
					}
				}
				shortestWay(originP, o);
			}
		}
	}

	public void print() {
		String penguin = "\uD83D\uDC27";
		for (int i = 0; i < height; i++) {

			System.out.print(i);
			if (i < 10) {
				System.out.print(" ");
			}

			if (i % 2 != 0) {
				System.out.print(" ");
			}
			for (int j = 0; j < width; j++) {
				if (this.getCell(new Position(i, j)).isFree()) {
					System.out.print("+");
				} else if (this.getCell(new Position(i, j)).isCat()) {
					System.out.print(penguin);
				} else {
					System.out.print("-");
				}
			}
			System.out.println();
		}

	}

	public Position getCenter() {
		return new Position(this.width / 2, this.height / 2);

	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Cat getCat() {
		return cat;
	}

	public void setCat(Cat cat) {
		this.cat = cat;
	}

}
