import java.util.Scanner;

public class CatTrap {
	public static void main(String[] args) {

		// TODO
		// escape
		// user input
		// clear screen
		// column numbers
		// cell unicode presentation

		Field field = new Field(11, 11);
		Scanner scan = new Scanner(System.in);
		clearScreen();
		field.print();
		while (true) {
			System.out.print("Please enter the x coordinate:");
			int a = scan.nextInt();
			System.out.print("Please enter the y coordinate:");
			int b = scan.nextInt();
			if (a >= 0 && a < field.getWidth() && b >= 0 && b < field.getHeight()) {
				Position p = new Position(b, a);
				if (field.getCell(p).isCat() == false && field.getCell(p).isFree() == true) {
					field.getCell(p).setFree(false);
					field.escape();
					clearScreen();
					field.print();
				} else {
					clearScreen();
					System.out.println("Cell is alerady marked or taken by the cat");
					field.print();
				}
				if (field.getCat().getPosition().getColumn() < 0
						|| field.getCat().getPosition().getColumn() >= field.getWidth()
						|| field.getCat().getPosition().getRow() < 0
						|| field.getCat().getPosition().getRow() >= field.getHeight()) {
					field.getCat().setEscaped(true);
				}
				if (field.getCat().isEscaped() == true)
					break;
			} else {
				clearScreen();
				System.out.println("unvalid coordinates");
				field.print();
			}
		}
		System.out.println("Cat Escaped");
		scan.close();
	}

	public static void clearScreen() {
		for (int i = 0; i < 100; i++) {
			System.out.println("");
		}

	}
}
